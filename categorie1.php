<?php
$title = "eCommerce Gaming : Catégorie 1";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="./resources/app.css"/>
</head>
<body>
<div class="topbar">
    <div class="topbar-wrapper">
        <a class="topbar-brand" href="./index.php">
            eCommerce<br/>
            Gaming
        </a>
        <div class="topbar-search">

        </div>
        <div class="topbar-customer">
            <a href="./account.php">Mon compte</a>
            <a href="./cart.php">Mon panier</a>
        </div>
    </div>
</div>
<div class="wrapper">
    <nav class="navbar">
        <a href="./index.php">Tous nos produits</a>
        <a href="./categorie1.php" class="active">Catégorie 1</a>
        <a href="./categorie2.php">Catégorie 2</a>
    </nav>
    <main class="content">
        <h1>Catégorie 1</h1>
    </main>
</div>
</body>
</html>