<?php
$title = "eCommerce Gaming : Mon panier";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="./resources/app.css"/>
</head>
<body>
<div class="topbar">
    <div class="topbar-wrapper">
        <a class="topbar-brand" href="./index.php">
            eCommerce<br/>
            Gaming
        </a>
        <div class="topbar-search">

        </div>
        <div class="topbar-customer">
            <a href="./account.php">Mon compte</a>
            <a href="./cart.php" class="active">Mon panier</a>
        </div>
    </div>
</div>
<div class="wrapper">
    <main class="content">
        <h1>Mon panier</h1>
    </main>
</div>
</body>
</html>